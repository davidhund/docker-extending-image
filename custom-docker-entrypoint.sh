#!/bin/bash
set -euo pipefail

# This Entrypoint simply runs the ORIGINAL entrypoint (in /usr/local/bin/)
# but overrides some stuff...
# https://stackoverflow.com/questions/41582365/add-a-new-entrypoint-to-a-docker-image

# Remove themes _from source_ (before org. entrypoint copies to /var/www/)
echo "DH: Removing default WP themes"
rm -rf /usr/src/wordpress/wp-content/themes/twenty*

# Testing Composer
echo "DH: Testing extra Composer"
composer --version


echo "DH: Running original entrypoint"
# /usr/local/bin/docker-entrypoint.sh
bash docker-entrypoint.sh "$@" # Running `bash` prevents premature `exit` (which `source` does...)

# NOTE: TODO:
echo "DH: Do we even get at this point?"

exec "$@"
