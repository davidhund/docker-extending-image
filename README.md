# Custom Wordpress Image

- Based on `wordpress:5.2.3-php7.3-fpm`
- But with custom entrypoint script..
- Removing default `twenty*` themes
- and optionally doing other stuff

## Original Image

- [View original Dockerfile](https://github.com/docker-library/wordpress/blob/master/php7.3/fpm/Dockerfile)
- [View original docker-entrypoint.sh](https://github.com/docker-library/wordpress/blob/master/php7.3/fpm/docker-entrypoint.sh)

## Build Image

- Make sure to ``chmod +x custom-docker-entrypoint.sh` before building!

```
docker build -t my-wordpress .
```

## Run Container

```
docker run -d --name my-wordpress-container my-wordpress
```

## Bash into container..

.. and see `wp-content/themes/` is empty...

```
docker exec -i -t my-wordpress-container /bin/bash
# In container...
ls -la ./wp-content/themes
# .. should only contain 1 index.php
```

## Run Composer

Take note that it's good practice to *not* run as Root:
https://hub.docker.com/_/composer/#filesystem-permissions

<!-- docker run --rm -it --volume $PWD:/app --user $(id -u):$(id -g) composer install -->
docker run --rm -it --volume $PWD:/app --user $(id -u):$(id -g) composer --version