# We extend the base wordpress:5.2.3-php7.3-fpm
FROM wordpress:5.2.3-php7.3-fpm

# Our docker-entrypoint.sh simply
# runs the original, and extends it!
COPY custom-docker-entrypoint.sh /usr/local/bin/

# Now copy in Composer from (multistage build!) 
# the official Composer image
# https://github.com/docker-library/php/issues/344#issuecomment-364843883
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

ENTRYPOINT ["custom-docker-entrypoint.sh"]
CMD ["php-fpm"]
